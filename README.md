# Simple-Trade-Bot-Flow
Init :
- B1: Import mnemonic và 5 nft id, khoảng giá tương ứng
- B2: Dùng 5 địa chỉ đầu của wallet tương ứng với mnemonic ở trên (index 0 -> 4)
- B3: Chuyển quyền 5 nft ở trên cho 5 ví ở bước 3
- B4: List 5 nft id ở trên lên marketplace

FLow lặp :
- B5: Dùng 5 địa chỉ tiếp theo của wallet tương ứng với mnemonic ở trên (index 5 -> 9) làm 5 địa chỉ mua nft đã list
- B6: List lại lên sàn 5 nft đã mua
- B7: Dùng 5 địa chỉ đầu mua lại 5 nft ở trên
- B8: List lại lên sàn 5 nft đã mua
- B9: Sleep 10 phút


# Run trade bot using hardhat task tradeNft
### Task Arguments
- marketplaceContract: Address of marketplace contracts
- randomAmount: K nfts to get randome for buying and selling
- sleep-time: Sleep time in milliseconds after each loops

### Example
- You must fill NFT data in file ./data/nft_data.csv firstly.
- You only need to fill collection address and nft id correctly, the code will update owner, listing price, seller
```shell
npm run start-trade -- --marketplaceContract=aura1sxfyrcqsymqq4zgllzd3gg209w2rq4rcaza3t8c3drzyfzhzk56q2t07zn --randomAmount=2 --sleepTime=5000
```

### Output
Log files are in CSV format and stored in log folder.
#### CSV Headers
- TRANSACTION_TYPE
- ADDRESS
- NFT_ID
- PRICE
- FEE
- TX_HASH
