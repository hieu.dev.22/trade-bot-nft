import { readFile } from "fs/promises"
import Axios from "axios"
import { SigningCosmWasmClient, MsgExecuteContractEncodeObject } from "@cosmjs/cosmwasm-stargate"
import { AccountData, DirectSecp256k1HdWallet, OfflineDirectSigner } from "@cosmjs/proto-signing"
import { stringToPath } from "@cosmjs/crypto"
import { toUtf8 } from "@cosmjs/encoding"
import _ from "lodash"
import BigNumber from "bignumber.js"
import * as dotenv from "dotenv"
const ACCESS_TOKEN = ""
dotenv.config()
// const fs = require("fs");
import fs from "fs"
// const csv = require("csv-parser");
import csv from "csv-parser"
// const createCsvWriter = require("csv-writer").createObjectCsvWriter;
import { createObjectCsvWriter } from "csv-writer"
const csvLogHeader = [
    { id: "index", title: "NO" },
    { id: "collectionAddress", title: "COlLECTION_ADDRESS" },
    { id: "tokenId", title: "NFT_ID" },
    { id: "sender", title: "OWNER" },
    { id: "minPrice", title: "MIN_PRICE" },
    { id: "maxPrice", title: "MAX_PRICE" },
    { id: "isListing", title: "IS_LISTING" },
    { id: "listingPrice", title: "LISTING_PRICE" },
    { id: "seller", title: "SELLER" },
]
const API_DOMAIN = process.env.API_DOMAIN || "https://apiapollo-devheli.devtest101.xyz"
const RPC = process.env.RPC || "https://testnet-rpc.helichain.com/"
const MARKETPLACE_CONTRACT =
    process.env.MARKETPLACE_CONTRACT || "heli1hzz0s0ucrhdp6tue2lxk3c03nj6f60qy463we7lgx0wudd72ctmsdajac3"
const COIN_DENOM = process.env.COIN_DENOM || "theli"
const NFT_FILE_PATH = process.env.MINT_NFT_FILE_PATH || "./data/mint_nft_data.csv"
const fileLogPath = `./log/nft-minted-${new Date().getTime()}.csv`
let csvLogWriter = getCSVLogWriter(fileLogPath);
let accounts: readonly AccountData[]
let signer: OfflineDirectSigner

const axiosInstance = Axios.create({
    timeout: 15000,
    headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${ACCESS_TOKEN}`,
    },
    responseType: "json",
    baseURL: API_DOMAIN,
})

interface NftData {
    index: number
    tokenId?: number
    tokenUri?: string
    collectionAddress: string
    nftName: string
    nftDescription: string
    imageName: string
    nftProperties?: any
}

async function start() {
    const result = await _initSigners()
    accounts = result.accounts
    signer = result.signer
    const signingClient = await SigningCosmWasmClient.connectWithSigner(RPC, signer)

    let listNfts = (await readListNftsFromCsv(NFT_FILE_PATH)) as any[]
    for (let i = 0; i < listNfts.length; i++) {
        const { tokenId, tokenUri } = await saveNftData(listNfts[i]);
        listNfts[i].tokenId = tokenId;
        listNfts[i].tokenUri = tokenUri;

        console.log("Minting NFT", listNfts[i]);
        await mintNft(accounts[0].address, listNfts[i], signingClient);
    }
}

async function saveNftData(data: NftData) {
    try {
        const response = await axiosInstance.post("/public/api/v1/nft/save-metadata", {
            image: data.imageName,
            name: data.nftName,
            collectionAddress: data.collectionAddress,
            description: data.nftDescription,
            nftProperties: data.nftProperties,
        })
    
        return response.data
    } catch (err: any) {
        console.error(`Error saving NFT ${data.index}: ${err.message}`)
        throw new Error(err.message);
    }
}

async function mintNft(sender: string, data: NftData, signingClient: SigningCosmWasmClient) {
    try {
        const msg = {
            mint_nft: {
                contract_address: data.collectionAddress,
                token_id: data.tokenId?.toString(),
                token_uri: data.tokenUri,
            },
        }

        const sendMsg: MsgExecuteContractEncodeObject = {
            typeUrl: "/cosmwasm.wasm.v1.MsgExecuteContract",
            value: {
                sender,
                contract: MARKETPLACE_CONTRACT,
                msg: toUtf8(JSON.stringify(msg)),
                funds: [],
            },
        }
        const tx = await signingClient.signAndBroadcast(sender, [sendMsg], {
            gas: "500000",
            amount: [
                {
                    denom: COIN_DENOM,
                    amount: "836",
                },
            ],
        })

        csvLogWriter.writeRecords([
            {
                index: data.index,
                collectionAddress: data.collectionAddress,
                tokenId: data.tokenId,
                sender,
                minPrice: "0",
                maxPrice: "0",
                isListing: "false",
                listingPrice: "0",
                seller: "",
            },
        ]);

        console.log(
            `Minted NFT ${data.index} with tokenId ${data.tokenId} and tokenUri ${data.tokenUri} with txHash ${tx.transactionHash}`
        )
    } catch (err: any) {
        console.error(`Error minting NFT ${data.index}: ${err.message}`)
    }
}

/**
 *  Read NFTs from csv file
 * @returns {Promise<{id: number, listingPrice: BigNumber}[]>}
 */
async function readListNftsFromCsv(filePath: string): Promise<{ id: number; listingPrice: BigNumber }[]> {
    return new Promise((resolve, reject) => {
        const nftData: any[] = []

        fs.createReadStream(filePath)
            .pipe(
                csv({
                    mapHeaders: ({ header, index }) => header.toLowerCase(),
                })
            )
            .on("data", (row) => {
                const nftProperties = { ...row };
                delete nftProperties.collection_address;
                delete nftProperties.nft_name;
                delete nftProperties.nft_description;
                delete nftProperties.image_name;
                delete nftProperties.no;

                const properties: {
                    traitType: string;
                    value: string;
                }[] = [];
                for (const key in nftProperties) {
                    properties.push({
                        traitType: key,
                        value: nftProperties[key],
                })
                }
                
                nftData.push({
                    index: nftData.length + 1,
                    collectionAddress: row.collection_address,
                    nftName: row.nft_name,
                    nftDescription: row.nft_description,
                    imageName: row.image_name,
                    nftProperties: properties,
                } as NftData)
            })
            .on("end", () => resolve(nftData))
            .on("error", (err) => reject(err))
    })
}

function sleep(ms: number) {
    return new Promise((resolve) => setTimeout(resolve, ms))
}

function fileExists(path: string) {
    try {
        fs.accessSync(path, fs.constants.F_OK)
        return true
    } catch (err) {
        return false
    }
}

function getCSVLogWriter(fileLogPath: string) {
    const csvWriter = createObjectCsvWriter({
        path: fileLogPath,
        header: csvLogHeader,
        append: true,
    })

    //Prepare File Log
    if (!fileExists(fileLogPath)) {
        // Write headers directly to the file
        fs.writeFileSync(fileLogPath, "NO,COlLECTION_ADDRESS,NFT_ID,OWNER,MIN_PRICE,MAX_PRICE,IS_LISTING,LISTING_PRICE,SELLER\n")
    }

    return csvWriter
}

async function _initSigners() {
    const hdPaths = Array.from({ length: 100 }, (_, index) => stringToPath(`m/44'/118'/0'/0/${index}`))
    const signer: OfflineDirectSigner = await DirectSecp256k1HdWallet.fromMnemonic(
        (await readFile("./testnet.my.mnemonic.key")).toString(),
        {
            hdPaths,
            prefix: process.env.COIN,
        }
    )

    return {
        accounts: await signer.getAccounts(),
        signer,
    }
}

async function createCollection(sender: string, signingClient: SigningCosmWasmClient) {
    const msg = {
        create_collection: {
            name: "test",
            symbol: "test",
        },
    }

    const sendMsg = {
        typeUrl: "/cosmwasm.wasm.v1.MsgExecuteContract", // This type should match the type in the registry
        value: {
            sender, // Replace with the sender's address
            contract: MARKETPLACE_CONTRACT,
            msg: toUtf8(JSON.stringify(msg)),
            funds: [],
        },
    }

    const tx = await signingClient.signAndBroadcast(sender, [sendMsg], {
        gas: "500000",
        amount: [
            {
                denom: COIN_DENOM,
                amount: "100",
            },
        ],
    })

    console.log(`Created collection with txHash ${tx.transactionHash}`, tx);
}

start().catch((error) => {
    console.error("Minting nft encountered an error:", error)
})
