import { readFile } from "fs/promises"
import { SigningCosmWasmClient, MsgExecuteContractEncodeObject } from "@cosmjs/cosmwasm-stargate"
import { AccountData, DirectSecp256k1HdWallet, OfflineDirectSigner } from "@cosmjs/proto-signing"
import { stringToPath } from "@cosmjs/crypto"
import { toUtf8 } from "@cosmjs/encoding"
import _ from "lodash"
import BigNumber from "bignumber.js"
import * as dotenv from 'dotenv';
dotenv.config();

// const fs = require("fs");
import fs from "fs"
// const csv = require("csv-parser");
import csv from "csv-parser"
// const createCsvWriter = require("csv-writer").createObjectCsvWriter;
import { createObjectCsvWriter } from "csv-writer"
const csvLogHeader = [
    { id: "transactionType", title: "TRANSACTION_TYPE" },
    { id: "address", title: "ADDRESS" },
    { id: "nftId", title: "NFT_ID" },
    { id: "price", title: "PRICE" },
    { id: "txHash", title: "TX_HASH" },
]
const RPC = process.env.RPC || "https://rpc.euphoria.aura.network";
const COIN_DECIMAL = parseInt(process.env.COIN_DECIMAL || "6");
const COIN_DENOM = process.env.COIN_DENOM || "ueaura";
const NFT_FILE_PATH = process.env.NFT_FILE_PATH || "./data/nft_data.csv";
const fileLogPath = `./log/log-${new Date().getTime()}.csv`;
let csvLogWriter: any;
let accounts: readonly AccountData[];
let signer: OfflineDirectSigner;

async function tradeBot() {
    const params = parseArguments()

    csvLogWriter = getCSVLogWriter(fileLogPath)
    const result = await _initSigners()
    accounts = result.accounts
    signer = result.signer
    const signingClient = await SigningCosmWasmClient.connectWithSigner(RPC, signer)
    const marketplaceContract = params.marketplaceContract
    const randomAmount = parseInt(params.randomAmount)
    const sleepTime = parseInt(params.sleepTime)

    let nftData = (await readListNftsFromCsv(NFT_FILE_PATH)) as any
    nftData = await updateNftData(nftData, signingClient, marketplaceContract)
    console.log(`[UpdateNftData] Update NFT data successfully!`)

    let loop = 1

    while (1) {
        console.log(`=====Loop ${loop++}=====`)

        const listNftToSell = getRandomNftToSell(nftData, randomAmount)
        if (listNftToSell.length === 0) {
            console.log(`-> [ListNFT] No NFTs to sell!`);
        }
        for (let i = 0; i < listNftToSell.length; i++) {
            const nft = listNftToSell[i]
            nft.listingPrice = getListingPrice(nft.minPrice, nft.maxPrice)

            const isApprovedForAll = await isApproved(
                signingClient,
                nft.collectionAddress,
                nft.id,
                marketplaceContract
            )

            // Get fee from account[0] to execute transactions
            await getTransFeeForListingNft(signingClient, accounts[0].address, nft.owner, isApprovedForAll)

            // 1. Approve for marketplace to transfer NFT
            if (!isApprovedForAll) {
                await approveForMarketplace(
                    marketplaceContract,
                    nft.collectionAddress,
                    nft.id,
                    nft.owner,
                    signingClient
                )
            } else {
                console.log(`-> [ListNFT] Marketplace is already approved to transfer NFT from ${nft.owner}.`)
            }

            // 2. List NFT to marketplace
            await listNft(
                marketplaceContract,
                nft.collectionAddress,
                nft.id,
                nft.listingPrice,
                nft.owner,
                signingClient
            )
        }
        nftData = await updateNftData(nftData, signingClient, marketplaceContract);

        const listRandomNftToBuy = getRandomNftToBuy(nftData, randomAmount)
        if (listRandomNftToBuy.length === 0) {
            console.log(`-> [ListNFT] No NFTs to buy!`);
        }
        for (let i = 0; i < listRandomNftToBuy.length; i++) {
            const nft = listRandomNftToBuy[i]
            const buyer = getRandomBuyer(listRandomNftToBuy[i].seller)

            if (!buyer){
                console.log(`-> [BuyNFT] Can't find buyer for NFT ${nft.id}!`)
                continue;
            }

            // Transfer TOKEN for buying NFT from root account to buyer
            await transferTokenToBuyNFT(signingClient, accounts[0].address, buyer.address, nft.listingPrice)

            // 3. Buy NFT
            await buyNft(
                signingClient,
                marketplaceContract,
                nft.collectionAddress,
                nft.id,
                buyer.address,
                nft.listingPrice
            )

            nftData[buyer.address] = nft
        }
        nftData = await updateNftData(nftData, signingClient, marketplaceContract);
        rewriteNftDataToCsv(nftData, NFT_FILE_PATH)
        
        // sleep for N seconds
        console.log(`=====SLEEP FOR ${sleepTime} MS=====`)
        await sleep(sleepTime)
    }
}

/**
 * Get listing price
 * @param {number} minPrice
 * @param {number} maxPrice
 * @returns
 */
function getListingPrice(minPrice: number, maxPrice: number) {
    minPrice = Math.ceil(minPrice * 10) / 10 // Round up to nearest 0.1
    maxPrice = Math.floor(maxPrice * 10) / 10 // Round down to nearest 0.1
    const numSteps = (maxPrice - minPrice) * 10
    const step = 0.1
    const randomStep = Math.floor(Math.random() * (numSteps + 1))
    const price = (minPrice + randomStep * step).toFixed(1)

    return price
}

/**
 *  Read NFTs from csv file
 * @returns {Promise<{id: number, listingPrice: BigNumber}[]>}
 */
async function readListNftsFromCsv(filePath: string): Promise<{ id: number; listingPrice: BigNumber }[]> {
    return new Promise((resolve, reject) => {
        const nftData: any[] = []

        fs.createReadStream(filePath)
            .pipe(
                csv({
                    mapHeaders: ({ header, index }) => header.toLowerCase(),
                })
            )
            .on("data", (row) => {
                if (!row.nft_id) return
                const maxPrice = parseFloat(row.max_price) || 0
                const minPrice = parseFloat(row.min_price) || 0
                const collectionAddress = row.collection_address
                if (maxPrice < minPrice) return

                nftData.push({
                    index: nftData.length + 1,
                    id: row.nft_id,
                    minPrice,
                    maxPrice,
                    collectionAddress,
                    owner: row.owner,
                    listingPrice: row.listing_price || "0",
                    isListing: row.is_listing === "true",
                    seller: row.seller || "",
                })
            })
            .on("end", () => resolve(nftData))
            .on("error", (err) => reject(err))
    })
}

async function buyNft(
    signingClient: SigningCosmWasmClient,
    marketplaceContract: string,
    collectionContract: string,
    tokenId: string,
    buyer: string,
    price: string
) {
    const listingInfo = await getListingInfo(signingClient, marketplaceContract, collectionContract, tokenId);
    if (!listingInfo) {
        console.log(`[BuyNFT] NFT ${tokenId} is not listed on marketplace!`)
        return
    }

    const msg = {
        buy: {
            contract_address: collectionContract,
            token_id: tokenId,
        },
    }

    const sendMsg: MsgExecuteContractEncodeObject = {
        typeUrl: "/cosmwasm.wasm.v1.MsgExecuteContract",
        value: {
            sender: buyer,
            contract: marketplaceContract,
            msg: toUtf8(JSON.stringify(msg)),
            funds: [
                {
                    denom: COIN_DENOM,
                    // NFT price here
                    amount: BigNumber(price)
                        .multipliedBy(10 ** COIN_DECIMAL)
                        .toFixed(),
                },
            ],
        },
    }

    const tx = await signingClient.signAndBroadcast(buyer, [sendMsg], {
        gas: "500000",
        amount: [
            {
                denom: COIN_DENOM,
                amount: "836",
            },
        ],
    })

    console.log(`--> [BuyNFT] Buy NFT id ${tokenId} price:${price} successfully.`)

    csvLogWriter.writeRecords([
        {
            transactionType: "Buy NFT from marketplace",
            address: buyer,
            price: price,
            // fee: ethers.utils.formatUnits(transactionFee),
            nftId: tokenId,
            txHash: tx.transactionHash,
        },
    ])
}

function sleep(ms: number) {
    return new Promise((resolve) => setTimeout(resolve, ms))
}

function fileExists(path: string) {
    try {
        fs.accessSync(path, fs.constants.F_OK)
        return true
    } catch (err) {
        return false
    }
}

function getCSVLogWriter(fileLogPath: string) {
    const csvWriter = createObjectCsvWriter({
        path: fileLogPath,
        header: csvLogHeader,
        append: true,
    })

    //Prepare File Log
    if (!fileExists(fileLogPath)) {
        // Write headers directly to the file
        fs.writeFileSync(fileLogPath, "TRANSACTION_TYPE,ADDRESS,NFT_ID,PRICE,FEE,TX_HASH\n")
    }

    return csvWriter
}

function getCSVNftDataWriter(fileLogPath: string) {
    const csvWriter = createObjectCsvWriter({
        path: fileLogPath,
        header: [
            { id: "index", title: "NO" },
            { id: "collectionAddress", title: "COlLECTION_ADDRESS" },
            { id: "id", title: "NFT_ID" },
            { id: "owner", title: "OWNER" },
            { id: "minPrice", title: "MIN_PRICE" },
            { id: "maxPrice", title: "MAX_PRICE" },
            { id: "isListing", title: "IS_LISTING" },
            { id: "listingPrice", title: "LISTING_PRICE" },
            { id: "seller", title: "SELLER" },
        ]
    })

    // //Prepare File Log
    // if (!fileExists(fileLogPath)) {
    //     // Write headers directly to the file
    //     fs.writeFileSync(fileLogPath, "NO,COlLECTION_ADDRESS,NFT_ID,OWNER,MIN_PRICE,MAX_PRICE,LISTING_PRICE,IS_LISTING,SELLER\n")
    // }

    return csvWriter
}

async function rewriteNftDataToCsv(nftData: any[], filePath: string) {
    const csvWriter = getCSVNftDataWriter(filePath)
    await csvWriter.writeRecords(nftData)
}

async function transferTokenToBuyNFT(
    signingClient: SigningCosmWasmClient,
    sender: string,
    receiver: string,
    listingPrice: string
) {
    const amount = BigNumber(listingPrice)
        .multipliedBy(10 ** COIN_DECIMAL)
        .plus(1000)
        .toFixed()

    await signingClient.sendTokens(sender, receiver, [{ denom: COIN_DENOM, amount }], {
        amount: [{ denom: COIN_DENOM, amount: "1000" }],
        gas: "200000",
    })

    console.log(
        `-> [BuyNFT] Transfer ${BigNumber(amount).div(
            10 ** COIN_DECIMAL
        )} Token for buying NFT from root account to buyer ${receiver} successfully`
    )
}

async function _initSigners() {
    const hdPaths = Array.from({ length: 100 }, (_, index) => stringToPath(`m/44'/118'/0'/0/${index}`))
    const signer: OfflineDirectSigner = await DirectSecp256k1HdWallet.fromMnemonic(
        (await readFile("./testnet.my.mnemonic.key")).toString(),
        {
            hdPaths,
            prefix: "aura",
        }
    )

    return {
        accounts: await signer.getAccounts(),
        signer,
    }
}

async function isApproved(
    signingClient: SigningCosmWasmClient,
    collectionAddress: string,
    tokenId: string,
    spender: string
) {
    try {
        const result = await signingClient.queryContractSmart(collectionAddress, {
            approval: {
                spender,
                token_id: tokenId,
            },
        })

        return !!result
    } catch (error: any) {
        console.log(`[ListNFT] Check is approved false for NFT Id ${tokenId}!`)
        return false
    }
}

async function getNFTOwners(
    signingClient: SigningCosmWasmClient,
    collectionAddress: string,
    tokenId: string
) {
    try {
        const result = await signingClient.queryContractSmart(collectionAddress, {
            owner_of: {
                token_id: tokenId,
            },
        })

        return result
    } catch (error: any) {
        return ""
    }
}

async function updateNftData(
    nftData: any[],
    signingClient: SigningCosmWasmClient,
    marketplaceAdress: string
) {
    for (let i = 0; i < nftData.length; i++) {
        const collectionAddress = nftData[i].collectionAddress
        const nftId = nftData[i].id
        const ownnerResult = await getNFTOwners(signingClient, collectionAddress, nftId)
        if (ownnerResult) {
            nftData[i].owner = ownnerResult.owner
        }

        const listingInfo = await getListingInfo(signingClient, marketplaceAdress, collectionAddress, nftId)
        if (listingInfo) {
            nftData[i].listingPrice = BigNumber(listingInfo.listing_config.price.amount)
                .div(10 ** COIN_DECIMAL)
                .toFixed()
            nftData[i].isListing = true
            nftData[i].seller = listingInfo.seller
        } else {
            nftData[i].isListing = false
            nftData[i].listingPrice = "0"
            nftData[i].seller = ""
        }
    }

    return nftData
}

function getRandomNftToSell(nftData: any[], amount: number) {
    const nftToSell = nftData.filter((nft) => {
        return nft.isListing === false && accounts.some((account) => account.address === nft.owner)
    })
    return _.sampleSize(nftToSell, amount)
}

function getRandomNftToBuy(nftData: any[], amount: number) {
    const nftToBuy = nftData.filter(
        (nft) => nft.isListing === true && accounts.some((account) => account.address === nft.owner)
    )
    return _.sampleSize(nftToBuy, amount)
}

function getRandomBuyer(sellerAddress: string) {
    const buyer = accounts.filter((account) => account.address !== sellerAddress)
    return _.sample(buyer)
}

async function getListingInfo(
    signingClient: SigningCosmWasmClient,
    marketplaceAdress: string,
    collectionAddress: string,
    nftId: string
) {
    try {
        const result = await signingClient.queryContractSmart(marketplaceAdress, {
            listing: {
                contract_address: collectionAddress,
                token_id: nftId,
            },
        })

        return result
    } catch (error: any) {
        return null
    }
}

async function getTransFeeForListingNft(
    signingClient: SigningCosmWasmClient,
    sender: string,
    receiver: string,
    isApprovedForAll: boolean
) {
    if (sender === receiver) return
    let feeAmount = 2000
    if (isApprovedForAll) {
        feeAmount += 1000
    }

    await signingClient.sendTokens(sender, receiver, [{ denom: COIN_DENOM, amount: feeAmount.toString() }], {
        amount: [{ denom: COIN_DENOM, amount: "1000" }],
        gas: "200000",
    })

    console.log(
        `-> [ListNFT] Transfer ${BigNumber(feeAmount).div(
            10 ** COIN_DECIMAL
        )} TOKEN for transaction fee from root account to seller ${receiver} successfully`
    )
}

async function approveForMarketplace(
    marketplaceContract: string,
    collectionContract: string,
    tokenId: string,
    buyer: string,
    signingClient: SigningCosmWasmClient
) {
    const msg = {
        approve: {
            spender: marketplaceContract,
            token_id: tokenId,
            expires: {
                never: {},
            },
        },
    }

    const sendMsg: MsgExecuteContractEncodeObject = {
        typeUrl: "/cosmwasm.wasm.v1.MsgExecuteContract",
        value: {
            sender: buyer,
            contract: collectionContract,
            msg: toUtf8(JSON.stringify(msg)),
        },
    }

    const tx = await signingClient.signAndBroadcast(buyer, [sendMsg], {
        gas: "500000",
        amount: [
            {
                denom: COIN_DENOM,
                amount: "836",
            },
        ],
    })

    console.log(`-> [ListNFT] Approve for marketplace to transfer NFT from ${buyer} successfully.`)

    csvLogWriter.writeRecords([
        {
            transactionType: "Approve Marketplace to transfer NFT",
            address: buyer,
            price: "",
            nftId: "",
            txHash: tx.transactionHash,
        },
    ])
}

async function listNft(
    marketplaceContract: string,
    collectionContract: string,
    tokenId: string,
    price: string,
    seller: string,
    signingClient: SigningCosmWasmClient
) {
    try {
        const msg = {
            list_nft: {
                contract_address: collectionContract,
                token_id: tokenId,
                listing_config: {
                    price: {
                        amount: BigNumber(price)
                            .multipliedBy(10 ** COIN_DECIMAL)
                            .toFixed(),
                        denom: COIN_DENOM,
                    },
                    start_time: null,
                    end_time: {
                        never: {},
                    },
                },
            },
        }

        const sendMsg: MsgExecuteContractEncodeObject = {
            typeUrl: "/cosmwasm.wasm.v1.MsgExecuteContract",
            value: {
                sender: seller,
                contract: marketplaceContract,
                msg: toUtf8(JSON.stringify(msg)),
            },
        }

        const tx = await signingClient.signAndBroadcast(seller, [sendMsg], {
            gas: "500000",
            amount: [
                {
                    denom: COIN_DENOM,
                    amount: "1000",
                },
            ],
        })

        csvLogWriter.writeRecords([
            {
                transactionType: "Lift NFT to marketplace",
                address: seller,
                price: price,
                nftId: tokenId,
                txHash: tx.transactionHash,
            },
        ])

        console.log(`[ListNFT] List NFT ${tokenId} successfully!`)
    } catch (error: any) {
        console.log(`[ListNFT] List NFT Id ${tokenId} to marketplace failed!`, error.message)
        return false
    }
}

function parseArguments() {
    // Skip the first two elements of process.argv (node path and script path)
    const args = process.argv.slice(2)

    // Create an object to hold the arguments
    const argumentsObject: Record<string, string> = {}

    // Iterate over the arguments and populate the object
    args.forEach((arg) => {
        // Each argument is expected to be in the format of --key=value
        const [key, value] = arg.split("=")
        if (key.startsWith("--") && value) {
            // Remove the '--' prefix from the key
            argumentsObject[key.slice(2)] = value
        }
    })

    return argumentsObject
}

tradeBot().catch((error) => {
    console.error("Trade bot encountered an error:", error)
})
